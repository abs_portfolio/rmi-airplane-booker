
	// Nick Tritsis (icsd11162), Nick Stergiopoulos(icsd11154), Giorgos Fiotakis (icsd13220)

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Flight implements Serializable {

    private int seats;
    private String to;
    private String from;

    private Date date;

    private String ID, name;    // ID == kwdikos pthshs || name == aeroplanou

    private ArrayList<Booking> bookings;
    // private int day, month, year;

    private int hour, minute;

    public Flight(String from, String to, Date date, int hour, int minute, String name, int seats) {
        this.to = to;
        this.from = from;
        this.date = date;
        this.hour = hour;
        this.minute = minute;

        this.name = name;
        this.seats = seats;
        setID();
        bookings = new ArrayList<Booking>();
        initializeBookings();

    }

    public void display() {
        System.out.println("Flight from " + from + " to " + to + " @ " + date + " (" + hour + "." + minute + ").");
        System.out.println("Airplane: " + name + ", Flight ID: " + ID + " Number of seats: " + seats);

    }

    public String getFrom() {
        return this.from;
    }

    public String getTo() {
        return this.to;
    }

    public Date getDate() {
        return this.date;
    }
    
    public String getDateToString() {
    	
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        int d, m, y;
    	
    	d = cal.get(Calendar.DAY_OF_MONTH);
    	m = cal.get(Calendar.MONTH);
    	y = cal.get(Calendar.YEAR);
    	
    	return "" + d + "/" + m + "/" + y;
    	
    }
    
    public String getTime() {
    	return "" + hour + ":" + minute;
    }

    public ArrayList<Booking> getAvailableSeats() {
        ArrayList<Booking> available = new ArrayList<Booking>();
        for (Booking booking : bookings) {
            if (booking.checkAvailable()) {
                available.add(booking);
            }
        }
        return available;
    }

    // CREATES STANDARD NON-PERSON BOOKING (availabilty is initialized and seatID is created here)
    private void initializeBookings() {

        String datID;

        char letter = 65;
        int countL = 1, countN = 0;
        int number, rows = seats / 4;

        for (int i = 0; i < seats; i++) {

            if (countL == 5) {					// GET LETTER BLOCK
                letter = 65;
                countL = 1;
            } else if (countL == 4) {
                letter = 68;
            } else if (countL == 3) {
                letter = 67;
            } else if (countL == 2) {
                letter = 66;
            } else if (countL == 1) {
                letter = 65;
            }

            countL++;							//

            number = (i) % 4;						// GET NUMBER BLOCK

            if (number == 0) {
                countN++;
            }

            number = countN;					//

            datID = "" + letter + number;		// FORM seatID STRING

            bookings.add(new Booking(datID));
            bookings.get(i).setSeatNum(i);
            bookings.get(i).setAvailability(true);

        }
       
                
    }

    private void setID() {
        this.ID = name + seats + hour + minute;
    }

    public String getID() {
        return ID;
    }

    /*public boolean addBooking(boolean temp, Person person, int num) {
        if (bookings.get(num).available) {
            bookings.get(num).setAvailability(person, false);
            return true;
        }
        return false;
    }*/
    
    public synchronized void bookSeat(Person person, int seat)//thema asfalias
    {
        if(!bookings.get(seat).getBooked())
        {
        bookings.get(seat).setBooked(true);
        bookings.get(seat).bookSeat(person);
        }else System.out.println("Is taken");
    }
    
    public boolean getBooked(int seat){
        return bookings.get(seat).getBooked();
    }

    public int checkSeats() {
        int avail = 0;
        for (Booking booking : bookings) {
            if (booking.checkAvailable()) {
                avail++;
            }
        }
        return avail;
    }

    public ArrayList<Booking> getBooking() {
        return this.bookings;
    }
    /*
     public static void main(String[] args) {
     Booking a = new Booking("Athens", "Amsterdam", new Date(), 20, 30);
     a.display();
     }
     */
    public void printBookedSeats(){
        for(Booking booking : bookings ){
            if (!booking.checkAvailable()) {
                System.out.println( "Seat : "+booking.getSeatId()+" has been booked by Name :"+booking.getPerson().getName() +" Surname : " +booking.getPerson().getSurName() );
            } 
        }
    }
}
