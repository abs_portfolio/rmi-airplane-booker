
	// Nick Tritsis (icsd11162), Nick Stergiopoulos(icsd11154), Giorgos Fiotakis (icsd13220)

import java.rmi.*;
import java.util.ArrayList;
import java.util.Date;

public interface ChatInterface extends Remote {
    
        public ArrayList<Flight> checkAvailability(String from , String to , Date date) throws RemoteException;
            
        public void bookMe(String name,String surname, int seet,String flightId) throws RemoteException;
        
        public ArrayList<Booking> checkId(String id) throws RemoteException;
        
        public Flight displayBook(String name, String surname,String id) throws RemoteException;
        
        public void temporaryReserveSeats(Booking temp, String flightId)throws RemoteException;
        
}