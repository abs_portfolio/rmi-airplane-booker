
	// Nick Tritsis (icsd11162), Nick Stergiopoulos(icsd11154), Giorgos Fiotakis (icsd13220)

import java.io.Serializable;

public class Person implements Serializable {

    private String name, surname;

    public Person(String name, String surname) {
        this.name = name;
        this.surname = surname;

    }

    public String getName() {
        return this.name;
    }

    public String getSurName() {
        return this.surname;
    }
}
