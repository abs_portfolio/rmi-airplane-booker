
	

import java.io.*;
import java.net.MalformedURLException;
import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ChatRMIServer extends UnicastRemoteObject implements ChatInterface {

    protected ArrayList<Flight> flights;

    Date date1 = new GregorianCalendar(2015, 03, 22).getTime();
    
    // RESOURCE
    Flight flight1 = new Flight("Athens", "Thessaloniki" 	, date1, 20, 10, "AC120", 14);
    Flight flight2 = new Flight("Spiti mou", "Spiti sou" 	, date1, 20, 10, "AC220", 8);
    Flight flight3 = new Flight("Wonderland", "Neverland" 	, date1, 20, 10, "AC320", 28);
    Flight flight4 = new Flight("Amsterdam", "Jamaica" 	, date1, 20, 10, "AC420", 24);
    Flight flight5 = new Flight("Auschwitz" , "Chernobyl" 	, date1, 20, 10, "AC520", 10);
    Flight flight6 = new Flight("Underworld", "Las Vegas" 	, date1, 20, 10, "AC720", 18);

	protected ObjectOutputStream out;
	protected ObjectInputStream in;
	
	protected File file = new File("Flights.txt");
	
	

    public ChatRMIServer() throws RemoteException {
        super();
        checkFile();
    }

	
    public void initializeFlights() {
        flights = new ArrayList<Flight>();
        flights.add(flight1);
        flights.add(flight2);
        flights.add(flight3);
        flights.add(flight4);
        flights.add(flight5);
        flights.add(flight6);
    }

    public String randomDestination() {
    	
    	Random randNums = new Random();
		int i = randNums.nextInt(2);
		
		String[] destinations = {"Seattle", "Liege", "Adelaine", "Casablanca",
											   "Siberia", "Sicily", "Tasmania", "Pittsburgh",
											   "Costa Rica", "Wales", "Rio de Janeiro", "Zagreb",
											   "Detroit", "Denver", "Havana", "Taipei", 
											   "Guatemala", "Frankfurt", "Richmond", "Guadalajara",
											   "Thessaloniki", "Amsterdam", "Pythagorio", "Jamaica",
											   "Athens", "Paris", "Venice", "Prague"};
		
		//return destinations[i];
        return destinations[i];
    }
    public void displayAll() {
        for (Flight flight : flights) {
            flight.display();
        }
    }

    // BOOKS A SEAT IN A FLIGHT
    @Override
    public synchronized void bookMe(String name, String surname, int seat,String flightId) throws RemoteException {
        System.out.println(seat);
        for (final Flight flight : flights) {
            if (flight.getID().equals(flightId)) {
                if (flight.getBooking().get(seat).getPerson() == null) {
                    System.out.println("den einai piasmenh h thesh :"+flight.getBooking().get(seat).getSeatId());
                    flight.getBooking().get(seat).bookSeat(new Person(name, surname));
                    System.out.println("Seat Booked :" + flight.getBooking().get(seat).getSeatId());
                }
            }
        }
        
        updateFile();
    }

    // RETURNS ARRAY LIST <FLIGHT> (ALL FLIGHTS THAT MATCH FROM-TO-DATE, THAT ARE GIVEN FROM USER)
    @Override
    public synchronized ArrayList<Flight> checkAvailability(String from, String to, Date date) throws RemoteException {
        
    	ArrayList<Flight> dayFlight = new ArrayList<Flight>();

        for (Flight flight : flights) {
            
        	if (flight.getFrom().equalsIgnoreCase(from) && flight.getTo().equalsIgnoreCase(to) && flight.getDate().equals(date)) {
                dayFlight.add(flight);
            }
        }

        return dayFlight;
    }

    // RETURN ARRAY LIST <BOOKING> (ALL SEATS IN A PARTICULAR FLIGHT)
    @Override
    public synchronized ArrayList<Booking> checkId(String id) throws RemoteException {
        
    	ArrayList<Booking> temp = new ArrayList<Booking>();
        
        for (Flight flight : flights) {
            
        	if (flight.getID().equals(id)) {
                temp = flight.getBooking();
            }
        }
        
        return temp;
    }

    // RETURN THE FLIGHT WHERE A PERSON HAS BOOKED HIS TICKET
    @Override
    public Flight displayBook(String name, String surname, String id) throws RemoteException {
        System.out.println(name + surname + id);
        Flight temp = null;
        for (final Flight flight : flights) {
            if (flight.getID().equals(id)) {
                for (Booking booking : flight.getBooking()) {
                    if (booking.getPerson() == null) {
                        continue;
                    } else if (booking.getPerson().getName().equalsIgnoreCase(name) && booking.getPerson().getSurName().equalsIgnoreCase(surname)) {
                        temp = flight;
                        break;
                    }else continue;
                }

            }
        }
        return temp;
    }

    // TEMPORARILY RESERVER SEAT FOR 2 MINUTES
    @Override
    public void temporaryReserveSeats(Booking temp, String flightId) throws RemoteException {
        boolean done = false;
        
        for (final Flight flight : flights) {
            if (flight.getID().equals(flightId)) {
                for (int i = 0; i < flight.getAvailableSeats().size(); i++) {
                    if (flight.getAvailableSeats().get(i).getSeatId().equals(temp.getSeatId())) {
                        
                    	System.out.println(i);
                        final ArrayList<Booking> books = flight.getAvailableSeats();
             
                        
                        final int index = i;
                        System.out.println("dat shit" +index);
                        
                        
                        
                        new java.util.Timer().schedule(
                                new java.util.TimerTask() {
                                    @Override
                                    public void run() {
                                        
                                        if (books.get(index).person == null) {
                                            if (!books.get(index).getBooked()) {
                                                books.get(index).setAvailability(true);
                                                System.out.println(books.get(index).getSeatId());
                                                System.out.println(index);
                                            }
                                            if (books.get(index).checkAvailable()) {
                                                System.out.println(books.get(index).getSeatId() + " is open again");
                                            } else {
                                                System.out.println(books.get(index).getSeatId() + " is temporary reserved");
                                            }
                                        }
                                        
                                        //checkInstanceShit(flightId,index);		// instance equality is checked here (systems out 1)
                                    }
                                },
                                120000
                        );
                        
                        books.get(index).setAvailability(false);
                        
                        System.out.println(index);
                        if (books.get(index).checkAvailable()) {
                        	System.out.println(books.get(index).getSeatId() + " is open");
                        } else {
                        	System.out.println(books.get(index).getSeatId() + " is temporary reserved");
                        }
                        
                    }
                }
            }
        }
    }
    
    // FILES
    // FILE CREATING/CHECKING/UPDATING METHODS
    // ========================================================
    private void checkFile() {

        System.out.println("SYSTEM: Checking for back-up files.");

        if (file.exists() == false) {

            try {
            	
            	initializeFlights();
            	
                file.createNewFile();

                out = new ObjectOutputStream(new FileOutputStream("Flights.txt"));

                out.writeObject(flights);

                out.flush();
                out.close();

            } catch (IOException ex) {
                System.out.println("Error: IOException while creating file");
                ex.printStackTrace();
            } finally {
                System.out.println("SYSTEM: New Flights file has been created.");
            }
        } else {
            rewriteFile();
        }

        System.out.println("SYSTEM: Files are in place & ready for IO.");

    }

    private void updateFile() {
        try {
            out = new ObjectOutputStream(new FileOutputStream("Flights.txt"));
            out.writeObject(flights);
            out.flush();


        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    private void rewriteFile() {

        System.out.println("SYSTEM: Users file exists. Updating values.");

        try {

            openInputStream("Flights.txt");
            flights = (ArrayList<Flight>) in.readObject();

        } catch (EOFException ex) {
            // System.out.println("Reservations EOF reached.");
        } catch (ClassNotFoundException ex) {
            System.out.println("Error casting");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Error: IO Exception");
            ex.printStackTrace();
        } finally {
            closeInputStream();
        }

        try {

            out = new ObjectOutputStream(new FileOutputStream("Flights.txt"));

            out.writeObject(flights);

        } catch (IOException ex) {
            System.out.println("Error: IO Exception");
            ex.printStackTrace();
        }

    }

    public void openInputStream(String Filename) {

        try {
            in = new ObjectInputStream(new FileInputStream(Filename));
        } catch (IOException ex) {
            System.out.println("Error: IOException on opening input stream");
            ex.printStackTrace();
        }
    }

    public void closeInputStream() {

        try {
            in.close();
        } catch (IOException ex) {
            System.out.println("Error: IOException on closing input stream");
            ex.printStackTrace();
        }

    }
	
	
	
	
	
	
	
    public static void main(String[] args) {

        // RMISecurityManager security = new RMISecurityManager();
        // System.setSecurityManager(security);
        ChatRMIServer server;
        try {
            server = new ChatRMIServer();
            
            Naming.rebind("//localhost/ChatRMI", server);
            System.out.println("Waiting new Messages");

        } catch (RemoteException ex) {
            ex.printStackTrace();
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        } 

        // OPEN CMD
        // CD TO LOCATION
        // start rmiregistry
        // rmic ChatRMIServer
        // kai meta run ap'to IDE
    }
}
