
	

import java.awt.Button;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.net.MalformedURLException;
import java.rmi.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class ChatRMIClient extends JFrame implements ActionListener {

    //System values
    protected ArrayList<Flight> dayFlight;
    ArrayList<Booking> a; 						
    
    //main frame
    protected JButton b1, b2, b3;
    protected JLabel main_bg;

    //check frame
    protected JFrame check_frame;
    protected JLabel check_bg;
    protected JButton okCheck;
    protected JTextField from, to, day, month, year;
    private final ChatInterface look_op;

    //book frame
    private JLabel book_bg;
    protected ArrayList<JButton> buttons;
    protected int button_index, ag;
    protected JFrame bookTicket;
    protected JTextField id, name, surname, seat;
    protected JButton okId, okBook, cancelBook, tempBook;
    protected String[] values = {"A1", "B1", "C1", "D1",
			"A2", "B2", "C2", "D2",
			"A3", "B3", "C3", "D3",
			"A4", "B4", "C4", "D4",
			"A5", "B5", "C5", "D5",
			"A6", "B6", "C6", "D6",
			"A7", "B7", "C7", "D7" };

    //showFlights
    protected JFrame show_tickets;
	protected JTextField show_from, show_to, show_ID, show_count;
	
	protected JButton show_next, show_previous;
	protected JLabel show_bg;
	
	protected int show_index;
    

    // ticketSearch
	protected JFrame searchTicket_frame;
	protected JLabel searchTicket_bg;
	protected JTextField searchTicket_name, searchTicket_surname, searchTicket_flightID;
    protected JButton okSearchTicket;
    
    // ticketInfo
    protected JFrame ticketInfo_frame;
    protected JLabel ticketInfo_bg;
    protected JLabel ticketInfo_fullName, ticketInfo_from, ticketInfo_to, ticketInfo_seatID, ticketInfo_date, ticketInfo_time, ticketInfo_cost;
    

    // IMAGE RESOURCES
    // BACKGROUNDS
    protected ImageIcon mainFrame = new ImageIcon(getClass().getResource("png/MainFrame.png"));
    protected ImageIcon bookFrame = new ImageIcon(getClass().getResource("png/BookFrame.png"));
    protected ImageIcon flightSearch = new ImageIcon(getClass().getResource("png/FlightSearch.png"));
    protected ImageIcon showFlights = new ImageIcon(getClass().getResource("png/showFlights.png"));
    protected ImageIcon ticketInfo = new ImageIcon(getClass().getResource("png/ticketInfo.png"));
    protected ImageIcon searchTicket = new ImageIcon(getClass().getResource("png/searchTicket.png"));
    protected ImageIcon tick = new ImageIcon(getClass().getResource("png/ticksmall.png"));
    
    protected ImageIcon s0 = new ImageIcon(getClass().getResource("png/defaultseat.png"));
    protected ImageIcon s1 = new ImageIcon(getClass().getResource("png/available.png"));
    protected ImageIcon s2 = new ImageIcon(getClass().getResource("png/taken.png"));
    
    // CONSTRUCTOR
    public ChatRMIClient() throws NotBoundException, MalformedURLException, RemoteException {
        
    	super("HADUKEN airport system");										// creating main frame
        setSize(900, 600);
        setLocationRelativeTo(null);
        setResizable(false);
        setLayout(null);

        look_op = (ChatInterface) Naming.lookup("//localhost/ChatRMI");			// initialize connection

        main_bg = new JLabel();
        main_bg.setSize(900, 600);
        main_bg.setIcon(mainFrame);
        this.add(main_bg);
        
        b1 = new JButton("Check Available");
        b1.setOpaque(false);
        b1.addActionListener(this);
        this.add(b1);
        b1.setBounds(650, 50, 200, 100);

        b2 = new JButton("Book flight");
        b2.setOpaque(false);
        b2.addActionListener(this);
        this.add(b2);
        b2.setBounds(650, 170, 200, 100);

        b3 = new JButton("Check Book");
        b3.setOpaque(false);
        b3.addActionListener(this);
        this.add(b3);
        b3.setBounds(650, 320, 200, 100);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        
    }

    private void FlightSearch() {
        check_frame = new JFrame("Flight Search");								// 1st button frame (check for a flight)
        check_frame.setSize(900, 600);
        check_frame.setResizable(false);
        check_frame.setLocationRelativeTo(null);
        check_frame.setLayout(null);

        from = new JTextField("");
        from.addActionListener(this);
        from.setBounds(650, 57, 180, 20);
        check_frame.add(from);

        check_bg = new JLabel();
        check_bg.setSize(900, 600);
        check_bg.setIcon(flightSearch);
        
        
        to = new JTextField("");
        to.addActionListener(this);
        to.setBounds(650, 87, 180, 20);
        check_frame.add(to);

        day = new JTextField("") {
            @Override
            public void processKeyEvent(KeyEvent ev) {
                char c = ev.getKeyChar();
                try {
                    // Ignore all non-printable characters. Just check the printable ones.
                    if (c > 31 && c < 127) {
                        Integer.parseInt(c + "");
                    }
                    super.processKeyEvent(ev);
                } catch (NumberFormatException nfe) {
                    System.err.println("SYSTEM: You can't enter chars into a date.");
                }
            }
        };
        day.addActionListener(this);
        day.setBounds(650, 143, 180, 20);
        check_frame.add(day);

        month = new JTextField("") {
            @Override
            public void processKeyEvent(KeyEvent ev) {
                char c = ev.getKeyChar();
                try {
                    // Ignore all non-printable characters. Just check the printable ones.
                    if (c > 31 && c < 127) {
                        Integer.parseInt(c + "");
                    }
                    super.processKeyEvent(ev);
                } catch (NumberFormatException nfe) {
                    // Do nothing. Character inputted is not a number, so ignore it.
                }
            }
        };
        month.addActionListener(this);
        month.setBounds(650, 173, 180, 20);
        check_frame.add(month);

        year = new JTextField("") {
            @Override
            public void processKeyEvent(KeyEvent ev) {
                char c = ev.getKeyChar();
                try {
                    // Ignore all non-printable characters. Just check the printable ones.
                    if (c > 31 && c < 127) {
                        Integer.parseInt(c + "");
                    }
                    super.processKeyEvent(ev);
                } catch (NumberFormatException nfe) {
                    // Do nothing. Character inputted is not a number, so ignore it.
                }
            }
        };
        year.addActionListener(this);
        year.setBounds(650, 203, 180, 20);
        check_frame.add(year);

        okCheck = new JButton();
        
        okCheck.setBounds(665, 248, 60, 60);
        
        okCheck.addActionListener(this);
        okCheck.setOpaque(false);
        
        
        check_frame.add(check_bg);
        check_frame.add(okCheck);
        check_frame.setVisible(true);

    }

    
    private void showFlights() {
    	
    	show_index = 0;
    	
    	show_tickets = new JFrame("Announcements Viewer");							// frame pop-up (shows all results for the previous frame)
    	show_tickets.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    	show_tickets.setVisible(true);
    	show_tickets.setResizable(false);
    	show_tickets.setSize(900, 600);
    	show_tickets.setLocationRelativeTo(null);

        Container pane = show_tickets.getContentPane();
        show_tickets.setLayout(null);
        
        show_bg = new JLabel();
        show_bg.setSize(900, 600);
        
        show_bg.setIcon(showFlights); 	
        
        
        show_from = new JTextField(dayFlight.get(show_index).getFrom());
        show_from.setBounds(415, 31, 138, 30);
        show_from.setEditable(false);
        
        show_to = new JTextField(dayFlight.get(show_index).getTo());
        show_to.setBounds(415,  66, 138, 30);
        show_to.setEditable(false);
        
        show_ID = new JTextField(dayFlight.get(show_index).getID());
        show_ID.setBounds(415, 100, 138, 30);
        show_ID.setEditable(false);
        
        show_previous = new JButton();
        show_previous.setBounds(348, 362, 35, 50);
        show_previous.setOpaque(false);
        
        show_next = new JButton();
        show_next.setBounds(523, 362, 35, 50);
        show_next.setOpaque(false);
        
        show_count = new JTextField(" " + (show_index+1) + "/" + dayFlight.size());
        show_count.setBounds(434, 370, 30, 30);
        
        // show_count.setBorder(null);
        show_count.setOpaque(false);
        show_count.setEditable(false);
        
		show_previous.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {

				if (show_index == 0) { 
					
				}
				else {
					show_index--;
				}
				
				show_from.setText(dayFlight.get(show_index).getFrom());
				show_to.setText(dayFlight.get(show_index).getTo());
				show_ID.setText(dayFlight.get(show_index).getID());
				
				show_count.setText(" " + (show_index+1) + "/" + dayFlight.size());

			}
		});

		show_next.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				if (show_index == dayFlight.size()-1) {	
					
				}
				else {
					show_index++;
				}

				show_from.setText(dayFlight.get(show_index).getFrom());
				show_to.setText(dayFlight.get(show_index).getTo());
				show_ID.setText(dayFlight.get(show_index).getID());
				
				show_count.setText(" " + (show_index+1) + "/" + dayFlight.size());

			}
		});
        
        pane.add(show_from);
        pane.add(show_to);
        pane.add(show_ID);
        pane.add(show_count);
        pane.add(show_bg);
        pane.add(show_previous);
        pane.add(show_next);
        
    }

    private void bookTicket_create() {
        bookTicket = new JFrame("Book A Ticket!");											// book ticket frame
        bookTicket.setSize(906, 628);
        bookTicket.setResizable(false);
        bookTicket.setLocationRelativeTo(null);
        
        bookTicket.setLayout(null);

        
        name = null;
        surname = null;
        id = null;

        
        book_bg = new JLabel();
        book_bg.setSize(900, 600);
        book_bg.setIcon(bookFrame);
        
        
        // ALL THE BUTTONS ARE HERE
        
        buttons = new ArrayList<JButton>();
        
        
        int x_inc = 0;									// algorithmos gia na mpainoun ta koumpia automata me vash thn thesh x, y tous
        int y_inc = 0;									// (ginetai gia na mhn ginoun 1-1 xeirokinhta
        
        int x = 253, y = 117;
        int cnti = 0, cntj = 0;
        
        boolean changeLine = false;
        
        for (int i = 0; i<28; i++) {
        	
        	buttons.add(i, new JButton());
        
        	
        	if (i<10)
        		buttons.get(i).setName("0" + i);
        	else 
        		buttons.get(i).setName("" + i);
        	
        	System.out.println(buttons.get(i).getName());
        	
        	if (cnti == 0) 
        		y_inc = 0;
        	else if (cnti == 1)
        		y_inc = 65;
        	else if (cnti == 2)
        		y_inc = 173;
        	else if (cnti == 3)
        		y_inc = 244;
        	else {
        		y_inc = 0;
        		cnti = 0;
        		cntj++;
        		changeLine = true;
        	}
        	
        	if (changeLine == true) {
        		x_inc = cntj*27 + cntj*60;
        		changeLine = false;
        	}											// ws edw
        	
        	buttons.get(i).setIcon(s0);
        	buttons.get(i).setBounds(x + x_inc, y + y_inc, 60, 60);
        	buttons.get(i).setBorder(null);
        	buttons.get(i).setOpaque(false);
        	buttons.get(i).setBackground(new Color(0,0,0,0));
        	buttons.get(i).setEnabled(false);
        	
        	
        	buttons.get(i).addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		
        		
        		ag = Integer.parseInt(e.getSource().toString().substring(20, 22));
        		tempBook.setEnabled(true);
        		okBook.setEnabled(true);
        		System.out.println(values[ag]);
        		
        		seat.setText(values[ag]);
        		
        		
				
			}});
        	
        	
        	bookTicket.add(buttons.get(i));
        	
        	cnti++;
        }
        
        
        
        //id
        id = new JTextField();	
        
        id.setBounds(177, 41, 120, 25);
        

        okId = new JButton();
        okId.setBounds(304, 41, 25, 25);
        okId.setBackground(new Color(0, 0, 0, 0));
        okId.setOpaque(false);
        okId.setBorder(null);
        okId.setIcon(tick);
        
        

        //book
        name = new JTextField();
        name.addActionListener(this);
        name.setBounds(177, 530, 120, 25);
        

        surname = new JTextField();
        surname.setBounds(447, 530, 120, 25);
        

        seat = new JTextField();
        seat.setBounds(177, 500, 120, 25);
        seat.setEditable(false);
        
        okBook = new JButton();
        okBook.setBounds(689, 496, 60, 60);
        okBook.setBackground(new Color(0,0,0,0));
        okBook.setOpaque(false);
        okBook.setBorder(null);
        okBook.setEnabled(false);
        
        cancelBook = new JButton();
        cancelBook.setBounds(774, 496, 60, 60);
        cancelBook.setBackground(new Color(0,0,0,0));
        cancelBook.setOpaque(false);
        cancelBook.setBorder(null);
        
        bookTicket.add(id);
        bookTicket.add(okId);
        bookTicket.add(name);
        bookTicket.add(surname);
        bookTicket.add(seat);
        bookTicket.add(okBook);
        bookTicket.add(cancelBook);
        bookTicket.add(book_bg);
        
        bookTicket.setVisible(true);
        
        id.addActionListener(this);
        okId.addActionListener(this);
        surname.addActionListener(this);
        seat.addActionListener(this);
        okBook.addActionListener(this);
        cancelBook.addActionListener(this);
    }

    private void searchTicket_create() {									// show analytical ticket frame
        
    	searchTicket_frame = new JFrame("Check Book");
        searchTicket_frame.setSize(900, 600);
        searchTicket_frame.setResizable(false);
        searchTicket_frame.setLocationRelativeTo(null);
        searchTicket_frame.setLayout(null);
        
        searchTicket_bg = new JLabel();
        searchTicket_bg.setSize(900, 600);
        searchTicket_bg.setIcon(searchTicket);
        
        searchTicket_name = new JTextField();
        searchTicket_name.addActionListener(this);
        searchTicket_name.setBounds(183, 70, 170, 25);
        searchTicket_frame.add(searchTicket_name);

        searchTicket_surname = new JTextField();
        searchTicket_surname.addActionListener(this);
        searchTicket_surname.setBounds(183, 100, 170, 25);
        searchTicket_frame.add(searchTicket_surname);
        
        searchTicket_flightID = new JTextField();
        searchTicket_flightID.addActionListener(this);
        searchTicket_flightID.setBounds(183, 157, 170, 25);
        searchTicket_frame.add(searchTicket_flightID);

        okSearchTicket = new JButton();
        okSearchTicket.addActionListener(this);
        okSearchTicket.setBounds(186, 256, 60, 60);
        okSearchTicket.setBackground(new Color(0, 0, 0, 0));
        okSearchTicket.setOpaque(false);
        okSearchTicket.setBorder(null);
        searchTicket_frame.add(okSearchTicket);
        
        searchTicket_frame.add(searchTicket_bg);

        searchTicket_frame.setVisible(true);

    }
    
    public void ticketInfo_create(Flight flight, String name, String surname) {
    	
    	ticketInfo_frame = new JFrame("Ticket");
    	ticketInfo_frame.setSize(900, 600);
    	ticketInfo_frame.setResizable(false);
    	ticketInfo_frame.setLocationRelativeTo(null);
    	ticketInfo_frame.setLayout(null);
        
    	ticketInfo_bg = new JLabel();
    	ticketInfo_bg.setSize(900, 600);
    	ticketInfo_bg.setIcon(ticketInfo);
        
    	ticketInfo_fullName = new JLabel(name + " " + surname);
        ticketInfo_fullName.setBounds(369, 228, 80, 15);
        ticketInfo_frame.add(ticketInfo_fullName);

        ticketInfo_from = new JLabel(flight.getFrom());
        ticketInfo_from.setBounds(369, 252, 80, 15);
        ticketInfo_frame.add(ticketInfo_from);
        
        ticketInfo_to = new JLabel(flight.getTo());
        ticketInfo_to.setBounds(369, 278, 80, 15);
        ticketInfo_frame.add(ticketInfo_to);

        String tempID = null;
        
        for (Booking booking : flight.getBooking()) {
        	if (booking.getPerson() != null) {
        		if (booking.getPerson().getName().equalsIgnoreCase(name) && booking.getPerson().getSurName().equalsIgnoreCase(surname))
        			tempID = booking.getSeatId();
        	}
        }
        
        ticketInfo_seatID = new JLabel(tempID);
        ticketInfo_seatID.setBounds(189, 348, 80, 15);
        ticketInfo_frame.add(ticketInfo_seatID);
       
        ticketInfo_date = new JLabel(flight.getDateToString());
        ticketInfo_date.setBounds(275, 348, 80, 15);
        ticketInfo_frame.add(ticketInfo_date);
        
        ticketInfo_time = new JLabel("" + flight.getTime());
        ticketInfo_time.setBounds(360, 348, 80, 15);
        ticketInfo_frame.add(ticketInfo_time);
        
        ticketInfo_cost = new JLabel("" + flight.getBooking().get(0).cost);
        ticketInfo_cost.setBounds(446, 348, 80, 15);
        ticketInfo_frame.add(ticketInfo_cost);
        
        ticketInfo_frame.add(ticketInfo_bg);

        ticketInfo_frame.setVisible(true);
    	
    	
    }

    @Override
    public void actionPerformed(ActionEvent e) {				// action performed
        if (e.getSource() == b1) {
            //this.setVisible(false);
            FlightSearch();
        }
        
        
        if (e.getSource() == b2) {
            this.setVisible(false);
            bookTicket_create();
        }
        
        
        if (e.getSource() == b3) {
            //this.setVisible(false);
            searchTicket_create();
        }
        
        
        if (e.getSource() == okCheck) {
            String tmp1, tmp2;
            tmp1 = this.from.getText();
            tmp2 = this.to.getText();

            int day1 = Integer.parseInt(day.getText());
            int month1 = Integer.parseInt(month.getText());
            int year1 = Integer.parseInt(year.getText());
            Date date1 = new GregorianCalendar(year1, month1, day1).getTime();
            dayFlight = null;
            dayFlight = new ArrayList<Flight>();
            try {

                dayFlight = look_op.checkAvailability(tmp1, tmp2, date1);
                if (!dayFlight.isEmpty()) {
                    
                    showFlights();
                    
                } else {
                	JOptionPane.showMessageDialog(null, "No flights found for this date!");
                }

            } catch (RemoteException ex) {
                ex.printStackTrace();
            }
            
            check_frame.dispose();

        }
        
        
        if (e.getSource() == okId) {
            
        	String temp = id.getText();
            id.setEditable(false);
            okId.setEnabled(false);
            try {

                a = new ArrayList();
                a = look_op.checkId(temp);

                for (int i = 0; i<a.size(); i++) {
                	if (a.get(i).checkAvailable()) {
                		buttons.get(i).setEnabled(true);
                		buttons.get(i).setIcon(s1);
                	}
                	else
                		buttons.get(i).setDisabledIcon(s2);
                		
                }
                
                if (a.size()<28) {
                	for (int i = 0; i<(28-a.size()); i++) {
                		buttons.get(i+a.size()).setVisible(false);
                		buttons.get(i+a.size()).setEnabled(false);
                	}
                }
               
                tempBook = new JButton();
                tempBook.setBounds(304, 500, 25, 25);
                tempBook.setBackground(new Color(0, 0, 0, 0));
                tempBook.setOpaque(false);
                tempBook.setIcon(tick);
                tempBook.setBorder(null);
                tempBook.setVisible(true);
                
                
                tempBook.addActionListener(this);
                
                // frame1.add(list);
                
                
                bookTicket.add(id);			
                
                bookTicket.add(name);
                bookTicket.add(surname);
                bookTicket.add(seat);
                bookTicket.add(okBook);
                
                bookTicket.add(tempBook);
                bookTicket.add(okId);
                
                bookTicket.add(book_bg);
                
                
                /*
                list.addActionListener(new ActionListener() {
                    
                    public void actionPerformed(ActionEvent e) {
                        index = list.getSelectedIndex();
                        System.out.println(index);

                        seat.setText("" + (index));
                    }
                });
                */

            } catch (RemoteException ex) {
                ex.printStackTrace();
            }
        }
        
        
        if (e.getSource() == okBook) {

            String temp = (String) name.getText();
            String temp2 = (String) surname.getText();
            String temp3 = (String) id.getText();

            id.setEditable(true);
            okId.setEnabled(true);

            try {
                look_op.bookMe(temp, temp2, ag, temp3);

            } catch (RemoteException ex) {
                ex.printStackTrace();
            }
            
            bookTicket.dispose();
            this.setVisible(true);
        }
        
        
        if (e.getSource() == cancelBook) {

            bookTicket.dispose();
            this.setVisible(true);
            
        }
        
        
        if (e.getSource() == this.okSearchTicket) {
            
        	
        	
        	String t = (String) searchTicket_flightID.getText();
            String temp = (String) searchTicket_name.getText();
            String temp2 = (String) searchTicket_surname.getText();
            
            Flight flight = null;
            
            try {
               flight = look_op.displayBook(temp, temp2, t);
               
               if (flight != null) {
            	   
            	   ticketInfo_create(flight, temp, temp2);
            	   flight.display();
               }
               else
            	   JOptionPane.showMessageDialog(null, "No ticket found!");
            } catch (RemoteException ex) {
                ex.printStackTrace();
            }
        }
        
        
        if (e.getSource() == this.tempBook) {
            
            tempBook.setEnabled(false);
            
            for (JButton button : buttons) {
            	button.setEnabled(false);
            }
            
            try {
                look_op.temporaryReserveSeats(a.get(ag) , id.getText());
            } catch (RemoteException ex) {
               ex.printStackTrace();
            }
            id.setEditable(true);
            
            
            

            
         }
    }

    public static void main(String[] args) throws NotBoundException, MalformedURLException, RemoteException {
        // RMISecurityManager security = new RMISecurityManager();
        // System.setSecurityManager(security);
        ChatRMIClient my = new ChatRMIClient();

    }

}
