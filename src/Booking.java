
	// Nick Tritsis (icsd11162), Nick Stergiopoulos(icsd11154), Giorgos Fiotakis (icsd13220)

import java.io.Serializable;

public class Booking implements Serializable {

    protected Person person;
    private String seatId;
    private int seatNum;

    protected final double cost = 30;

    protected boolean available ;
    protected boolean Booked =false;

    public Booking(String id) {
        seatId = id;
        person = null;
    }

    public void setSeatNum(int i) {
    	seatNum = i;
    }
    /*public void setAvailability(Person person, boolean temp) {
        available = temp;
        this.person = person;
        available = true;
    }*/
    protected void setBooked(boolean temp){
        this.Booked = temp;
    }
    public boolean getBooked(){
        return Booked ;
    }
    
    public void setAvailability(boolean temp) {//asfalia
        available = temp;
    }
    
    public String getSeatId(){
        return this.seatId;
    }
    public boolean checkAvailable() {
        return this.available;
    }
    
    public Person getPerson(){
        return this.person;
    }
     public void bookSeat(Person p){
        available = false; 
        person = p;
    }
}
