# Ticket Booking system based on RMI (Remote Method Invocation)
RMI Server Client walkthrough 
In order to run an RMI project you will need the bellow

1. In order to run the rmi server and create the stub class for the server .You can do it in several ways i prefer by cmd(comand line) for windows or terminal for linux.
2. In order to generate the stub files you need to compile the server class. Compile comand for all java files is javac. So javac ChatRMIServer.java and your projects path (like:: cd C/.....) enter the folder and javac the servers class (like:: javac RMIServer.java)
3. Now we need to open an rmi registry like this::      start rmiregistry on windows or rmiregistry on linux & (debian based os). More information about the RMI registry on different systems can be found here : https://docs.oracle.com/javase/tutorial/rmi/running.html.
4. Now you are ready to run the programm from your IDE run the server first and then the client. This project was build using Netbeans IDE. 

This project consists from a client and a server based on Remote Method Invocation (RMI) protocol. The server is responsible for the distribution and excecution of the services while the client is responsible of bringing theese services to the user with a nice interface. 
The second main demonstration of this project is the thread usage in order to monitor the ticket bookings.

This project does not contain a database to hold the flights, the users and every other essential information. In this case this information is hardcoded on the code. That's because this project is a demonstration of the RMI protocol and not a complete Ticket Booking System.
That being said, in class ChatRMIServer we can see the initialization of the flights. This are the flights that are available through the search too. Feel free to modify the code to add new flights etc. 

For the storage of the information about the flights booked files have beeing used. In theese files Objects are stored. Theese objects represents all the essential system's information.

*Screenshots from the application and discription.*
This is the client inteface. more specifically this is the home page of the clients inteface. It visualizes the 3 main services provided by the system. 

1.  Search a flight
2.  Book/Reserve a flight
3.  Visualize the booking.

The bookings are beiing stored in the form of class objects on files. From which on server start-up we obtain the previus bookings. That way server has always the list of previus bookings without the need of a Database.

![Home Scrn](./screenshots/home_screen.png)

The next Image is the booking frame. In this frame the user has the ability to book a flight by it's inique flight-id. In order to obtain the flight-id the user needs first to search for that flight by providing the To & Destination as well as the date.
After the aquisition of the flight Id the user can book a seat if there is any available on the plain. The available seat is  displaied with green while the booked seats are displaied whith red. The seats are actually buttons which if not booked the user can click in order to select and book them.
There is an option for a seat reserversion for 2 minutes. This action is hundled by a thread controller which is responsible for the seat booking. 

![Home Scrn](./screenshots/book.png)

For any clarifications or bugs please contact me on g.fiotakis@hotmail.com